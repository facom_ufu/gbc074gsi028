Curso de Sistemas Distribuídos
======

Cobre as disciplinas:

* GBC074 -- BCC
* GSI028 -- BSI

Para ler as notas de aula, vá para [markdown](./markdown)

Para versão LaTeX, sendo *depracated* vá para [latex](./latex)

Para acessar o código dos laboratórios diretamente, vá para [lab](./lab)

Para acessar material usado como referência, vá para [refs](./refs)
