# Distributed Map
Distributed map: Exercício usando DistributedMap, Version 3.10.0


git clone https://github.com/pluxos/atomix_labs

cd atomix_labs

cd replication

mvn compile

mvn test

## Run
### Servers
mvn exec:java -Dexec.mainClass="atomix_lab.atomix_lab.map.server.Server" -Dexec.args="0 127.0.0.1 5000 127.0.0.1 5001 127.0.0.1 5002"

mvn exec:java -Dexec.mainClass="atomix_lab.atomix_lab.map.server.Server" -Dexec.args="1 127.0.0.1 5000 127.0.0.1 5001 127.0.0.1 5002"

mvn exec:java -Dexec.mainClass="atomix_lab.atomix_lab.map.server.Server" -Dexec.args="2 127.0.0.1 5000 127.0.0.1 5001 127.0.0.1 5002"

### Clients
mvn exec:java -Dexec.mainClass="atomix_lab.atomix_lab.map.client.DistributedMapClient" -Dexec.args="0 127.0.0.1 5000 127.0.0.1 5001 127.0.0.1 5002"
